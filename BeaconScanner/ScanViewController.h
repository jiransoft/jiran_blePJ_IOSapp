//
//  ScanViewController.h
//  BeaconScanner
//
//  Created by SilverNine on 2014. 8. 10..
//  Copyright (c) 2014년 B-Conner. All rights reserved.
//

@import UIKit;

@interface ScanViewController : UITableViewController


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (weak, nonatomic) IBOutlet UITextView *textField;
@property (weak, nonatomic) IBOutlet UITextField *textaccount;
@property (weak, nonatomic) IBOutlet UITextField *textaddress;


@end
